# Coding in Go

## About the course

Go is a relatively new and popular programming language that supports source code compilation into native (machine) code, so the result is fast and memory-efficient applications comparable to the results produced by C, C++, D or Rust compilers. At the same time the Go language uses automatic memory management (GC - garbage collector), so-called goroutines and channels. At the same time Go is based on relatively minimalist syntax, which makes it quite significantly different from C or C++. Currently, Go is mainly used in containers and microservices ecosystems.

### Objectives

To inform course participants with all the important features of the Go programming language in such depth that they will be able to create and modify applications written in Go, while the resulting code will be effective and written in an idiomatic way.

### Outline

1. Elementary information about Go language
    - Origins of Go, Go versions
    - Go language use cases
    - Common features with other languages
    - Differences between Go and other languages
1. Go syntax and semantic
    - Keywords
    - Data types
    - Functions, functions visibility from other packages
    - Data structures
    - Interfaces
    - Program blocks
    - Variable visibility
    - Conditions
    - Program loops
    - Error handling
    - Operators
    - Arrays
    - Associative arrays (maps)
1. Concurrency and parallelism in Go
    - Goroutines
    - Channels - data structures used to communicate between goroutines
1. Practical examples
    - Working with sockets
    - HTTP server developed in Go
1. Go in real world
    - Testing
    - Benchmarking
    - Monitoring
1. Additional topics
    - Linters for Go
    - (Cross)compilation for different CPU architectures and for different OS

### Schedule

- 2023-09-19  First presentation
- 2023-10-03  Second presentation
- 2023-10-10  Third presentation
- 2023-10-17  Fourth presentation + deadline to choose PR/project/group project
- 2023-10-24  Fifth presentation
- 2023-10-31  Sixth presentation
- 2023-11-28  Projects presentation
- 2023-12-05  Projects deadline

### Lectors

- Ivan Nečas <inecas@redhat.com>
- Pavel Tišnovský <ptisnovs@redhat.com>

## Prerequisities

1. C language knowledge at middle level
1. Python language knowledge is optional, but help you much

## Learning materials

### Slides

Slides are available on https://github.com/RedHatOfficial/GoCourse

Please note that you need to have Go already installed in order to preview slides.

### Other materials

1. Tutorial: Get started with Go
https://go.dev/doc/tutorial/getting-started

1. Go doc
https://go.dev/doc/

1. The Why of Go (put many Go-related things into context)
www.youtube.com/watch?v=bmZNaUcwBt4&list=WL

1. GothamGo 2018 - Things in Go I Never Use by Mat Ryer
www.youtube.com/watch?v=5DVV36uqQ4E 


## Credits etc.

### Project

You can choose:

- it would be enough to have some PRs accepted in any (known) public repository. We can discuss about this one face to face if anyone is interested in this approach
- any project (written in Go) you are interested in AND have acks from Lectors
- larger project for group of students (again the project description needs to have acks from Lectors)
